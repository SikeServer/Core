# SikeServer-Core

## 概要
SikeServerのコアプラグインです。
https://www.sikeserver.com/

## 機能
・プレイヤーがサーバーにログインした際、Webサイトへの案内を表示します。

NEW! ・MOTDをConfigでいじれるようになります。

## 変更履歴
v1.0.1　MOTD機能追加

v1.0.0　最初のリリース
