package com.sikeserver.core;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.core.util.ConfigManager;
import com.sikeserver.core.util.SQLManager;

public class CoreManager extends JavaPlugin {
	private static CoreManager plugin;
	public static String playerTable = "core_players";
	public static String shopTable = "shop_shops";

	public ConfigManager conf;
	public SQLManager sql;

	/**
	 * プラグインが有効化された時に呼び出されるイベント
	 * 
	 * @author Siketyan
	 */
	@Override
	public void onEnable() {
		plugin = this;

		try {
			conf = new ConfigManager("config.yml", this);
		} catch (Exception e) {
			getLogger().warning("Error occured while loading config file!");
			e.printStackTrace();
		}

		try {
			// SQLManagerの新しいインスタンス作成（接続）
			sql = new SQLManager("mysql",
				conf.getString("MySQL.Host"),
				conf.getInt("MySQL.Port"),
				conf.getString("MySQL.Database"),
				conf.getString("MySQL.User"),
				conf.getString("MySQL.Password"),
				conf.getInt("MySQL.Timeout"));

			/*
			 * SQLテーブルの確認および作成
			 */

			// playerTable
			DatabaseMetaData meta = sql.getMetaData();
			getLogger().info("Loading `" + playerTable + "` table...");
			ResultSet rs = meta.getTables(null, null, playerTable, null);
			if (!rs.next()) {
				getLogger().info("Creating `" + playerTable + "` table...");
				sql.executeUpdate(
					"CREATE TABLE " + playerTable
						+ "(id INTEGER NOT NULL AUTO_INCREMENT,"
						+ " uuid VARCHAR(255),"
						+ " mcid VARCHAR(255),"
						+ " lastlogin VARCHAR(255),"
						+ " money BIGINT,"
						+ " authcode VARCHAR(16),"
						+ " authed INTEGER(1),"
						+ " PRIMARY KEY (id))");
				getLogger().info("Created `" + playerTable + "` table!");
			} else {
				getLogger().info("Loaded `" + playerTable + "` table!");
			}
			
			// shopTable
			getLogger().info("Loading `" + shopTable + "` table...");
			ResultSet rs2 = meta.getTables(null, null, shopTable, null);
			if (!rs2.next()) {
				getLogger().info("Creating `" + shopTable + "` table...");
				sql.executeUpdate(
					"CREATE TABLE " + shopTable
						+ "(x INTEGER,"
						+ " y INTEGER,"
						+ " z INTEGER,"
						+ " owner VARCHAR(255))");
				getLogger().info("Created `" + shopTable + "` table!");
			} else {
				getLogger().info("Loaded `" + shopTable + "` table!");
			}
		} catch (SQLException e) {
			getLogger().warning("The method throwed exception(s).");
			e.printStackTrace();
			Bukkit.getPluginManager().disablePlugin(this); // プレイヤーが入れなくなるのを防ぐため無効化
		}
	}

	/**
	 * CoreManagerのConfigからString値を取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @param key
	 *            Config上のパス
	 * 
	 * @return 文字列
	 */
	public String getMessage(String key) {
		return ChatColor.translateAlternateColorCodes('&', conf.getString(key));
	}

	/**
	 * CoreManagerのConfigからStringListを取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @param key
	 *            Config上のパス
	 * 
	 * @return StringList
	 */
	public List<String> getMessageList(String key) {
		return conf.getStringList(key);
	}
	
	/**
	 * UUIDからユーザー名を取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @param uuid UUID
	 * 
	 * @return ユーザー名
	 * 
	 * @throws SQLException
	 */
	public String getName(UUID uuid) throws SQLException {
		try (Statement stmt = sql.getStatement();
			 ResultSet result = stmt.executeQuery(
				"SELECT mcid FROM " + CoreManager.playerTable + " WHERE uuid = \"" + uuid.toString() + "\""
			 )
		) {
			result.next();
			return result.getString("mcid");
		}
	}

	/**
	 * ユーザー名の大文字/小文字を正しく表記しなおします。
	 * 
	 * @author Siketyan
	 * 
	 * @param name
	 *            ユーザー名
	 * 
	 * @return 大文字/小文字が正しいユーザー名
	 * 
	 * @throws SQLException
	 */
	public String getNameWithCase(String name) throws SQLException {
		try (Statement stmt = sql.getStatement();
			 ResultSet result = stmt.executeQuery(
				"SELECT mcid FROM " + CoreManager.playerTable + " WHERE mcid = \"" + name + "\""
			 ))
		{
			result.next();
			return result.getString("mcid");
		}
	}

	/**
	 * CoreManagerを取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @return CoreManagerぽいっ
	 */
	public static CoreManager getPlugin() {
		return plugin;
	}
}
