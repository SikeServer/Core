package com.sikeserver.core.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Yamlファイルを使用するためのユーティリティクラス
 * 
 * @author romown
 *
 */
public class ConfigManager extends YamlConfiguration {
	private final File file;
	private final JavaPlugin plugin;

	/**
	 * Yamlファイルを作成、及びロードします。
	 * 
	 * @author romown
	 */
	public ConfigManager(String file, JavaPlugin plugin)
			throws FileNotFoundException, IOException, InvalidConfigurationException {
		this.file = new File(plugin.getDataFolder(), file);
		this.plugin = plugin;
		load();
	}

	/**
	 * Yamlファイルをロードします。ファイルの場所は指定されたJavaPluginのデータフォルダになります。
	 * データフォルダが存在しない場合は作成されます。
	 * ファイルが存在しない場合は、リソースから読み込まれます。リソースにない場合は、空のファイルが作成されます。
	 * 
	 * @author romown
	 * 
	 * @return ファイルが新規作成された場合はtrue、それ以外はfalse
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws InvalidConfigurationException
	 */
	public boolean load() throws FileNotFoundException, IOException, InvalidConfigurationException {
		File dataFolder = plugin.getDataFolder();// データフォルダを取得
		if (!dataFolder.exists()) {// 存在しなかったら作る
			dataFolder.mkdir();
		}
		final boolean createNew;
		if (!file.exists()) {// 存在しなかった場合
			createNew = true;
			try (InputStream stream = plugin.getResource(file.getName())) {
				if (stream == null) {// Resourceにも存在しなかったら作成
					file.createNewFile();
				} else {
					Files.copy(stream, Paths.get(file.toURI()));// 存在したらリソースからコピー
				}
			}
		} else {
			createNew = false;// 新規作成してないのでfalse
		}
		load(file);// ロードして値を返す
		return createNew;
	}

	/**
	 * Yamlをセーブします。保存場所はコンストラクタで指定された場所依存になります。
	 * 
	 * @author romown
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		save(file);
	}
}
