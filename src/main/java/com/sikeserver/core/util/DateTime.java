package com.sikeserver.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTime {
	public static String getDateTime(String pattern) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);
	}
}
