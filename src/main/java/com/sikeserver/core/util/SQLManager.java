package com.sikeserver.core.util;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * SQLを使用するためのユーティリティクラス
 * 
 * @author romown
 *
 */
public class SQLManager implements Closeable {
	private Connection connection;// インターフェースなので継承できない
	private final String type;
	private final String host;
	private final int port;
	private final String database;
	private final String user;
	private final String password;
	private final int timeout;

	/**
	 * SQLにロードします。
	 * 
	 * @author romown
	 * 
	 * @param sql
	 *            使用するSQL
	 * @param host
	 *            ホスト
	 * @param port
	 *            ポート
	 * @param database
	 *            データベース
	 * @param user
	 *            ユーザー
	 * @param password
	 *            パスワード
	 * @param timeout
	 *            接続を検証するために使用したデータベース操作の完了を待機する秒数
	 * 
	 * @throws SQLException
	 */
	public SQLManager(String sql, String host, int port, String database, String user, String password, int timeout)
			throws SQLException {
		this.type = sql;
		this.host = host;
		this.port = port;
		this.database = database;
		this.user = user;
		this.password = password;
		this.timeout = timeout;
		connection = DriverManager.getConnection("jdbc:" + sql + "://" + host + ":" + port + "/" + database, user,
				password);
	}

	/**
	 * 指定されたデータベースのURLへの接続を試みます。 接続先はコンストラクタで指定された値になります。
	 * 
	 * @author romown
	 * 
	 * @throws SQLException
	 */
	public void connect() throws SQLException {
		connection = DriverManager.getConnection("jdbc:" + type + "://" + host + ":" + port + "/" + database, user,
				password);
	}

	/**
	 * 単一のResultSetオブジェクトを返す、指定されたSQL文を実行します。 コネクションが無効の場合は、接続してからSQL文を実行します。
	 * この際、コンストラクタで指定されたタイムアウトの秒数以下I/Oブロックが発生します。
	 * 
	 * @author romown
	 * 
	 * @param データベースに送られるSQL文。通常静的SQL
	 *            SELECT文
	 * 
	 * @return 指定されたクエリーによって作成されたデータを含むResultSetオブジェクト。nullにはならない
	 * 
	 * @throws SQLException
	 */
	@Deprecated
	public ResultSet executeQuery(String sql) throws SQLException {
		checkTimeout();
		try (Statement state = connection.createStatement()) {
			return state.executeQuery(sql);
		}
	}

	/**
	 * 指定されたSQL文を実行します。SQL文は、INSERT文、UPDATE文、DELETE文、またはSQL
	 * DDL文のような何も返さないSQL文の場合があります。 注:
	 * このメソッドをPreparedStatementまたはCallableStatementに対して呼び出すことはできません。
	 * コネクションが無効の場合は、接続してからSQL文を実行します。この際、コンストラクタで指定されたタイムアウトの秒数以下I/Oブロックが発生します。
	 * 
	 * @author romown
	 * 
	 * @param sql
	 * 
	 * @return SQLデータ操作言語(DML)文の場合は行数、何も返さないSQL文の場合は0
	 * 
	 * @throws SQLException
	 */
	public int executeUpdate(String sql) throws SQLException {
		checkTimeout();
		try (Statement state = connection.createStatement()) {
			return state.executeUpdate(sql);
		}
	}

	/**
	 * 直前のコミット/ロールバック以降に行われた変更をすべて永続的なものにし、このConnectionオブジェクトが現在保持するデータベース・
	 * ロックをすべて解除します。このメソッドは自動コミット・モードが無効になっているときしか使用できません。
	 * 
	 * @author romown
	 * 
	 * @throws SQLException
	 */
	public void commit() throws SQLException {
		connection.commit();
	}

	/**
	 * この接続の自動コミット・モードを指定された状態に設定します。接続が自動コミット・モードの場合、そのすべてのSQL文は実行され、
	 * 個別のトランザクションとしてコミットされます。
	 * 
	 * @author romown
	 * 
	 * @param autoCommit
	 * 
	 * @throws SQLException
	 */
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		connection.setAutoCommit(autoCommit);
	}

	/**
	 * このConnectionオブジェクトの現在の自動コミット・モードを取得します。
	 * 
	 * @author romown
	 * 
	 * @return このConnectionオブジェクトの現在の自動コミット・モードの状態
	 * 
	 * @throws SQLException
	 */
	public boolean getAutoCommit() throws SQLException {
		return connection.getAutoCommit();
	}

	/**
	 * 接続を閉じます。
	 * 
	 * @author romown
	 * 
	 * @throws IOException
	 */
	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * SQLとの接続が切れていたら再接続します。
	 * 
	 * @author Siketyan
	 * 
	 * @throws SQLException
	 */
	private void checkTimeout() throws SQLException {
		if (!connection.isValid(timeout)) {
			connect();
		}
	}

	/*
	 * 以下、他クラスからの呼び出し支援。
	 */

	/**
	 * コネクションのgetMetaData()をそのまま返します。
	 * 
	 * @author Siketyan
	 * 
	 * @return メタデータ
	 * 
	 * @throws SQLException
	 */
	public DatabaseMetaData getMetaData() throws SQLException {
		checkTimeout();
		return connection.getMetaData();
	}

	/**
	 * 新しいステートメントを作成して返します。
	 * 
	 * @author Siketyan
	 * 
	 * @return 新しいステートメント
	 * 
	 * @throws SQLException
	 */
	public Statement getStatement() throws SQLException {
		checkTimeout();
		return connection.createStatement();
	}
}
